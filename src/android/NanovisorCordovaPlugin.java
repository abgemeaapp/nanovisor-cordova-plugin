package com.instartlogic.nanovisor.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import com.instartlogic.nanovisor.Nanovisor;
import com.instartlogic.nanovisor.NanovisorStatus;
import com.instartlogic.nanovisor.NanovisorStatusListener;
import com.instartlogic.nanovisor.analytics.event.EventException;
import com.instartlogic.nanovisor.analytics.event.ObjectLoadEvent;
import android.util.Log;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class echoes a string called from JavaScript.
 */
public class NanovisorCordovaPlugin extends CordovaPlugin {

    private static final String TAG = NanovisorCordovaPlugin.class.getSimpleName();

    private CallbackContext statusListenerCallback = null;
    private CallbackContext sessionIDListenerCallback = null;

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        Log.e(TAG, "in native \"execute\" method now");
        if (action.equals("startNanovisor")) {
            final String license = args.getString(0);
            final Long dprSwitchArg = args.getLong(1);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.startNanovisor(license, dprSwitchArg, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("getStatus")) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.getStatus(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("getSessionID")) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.getSessionID(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("getNanovisorUrl")) {
            final String url = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.getNanovisorUrl(url, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("setStatusChangeListener")) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.setStatusChangeListener(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("setSessionIDChangeListener")) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.setSessionIDChangeListener(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("reportEvent")) {
            final Long startTime = args.isNull(0)   ? null : args.getLong(0);
            final Long t1Time = args.isNull(1)      ? null : args.getLong(1);
            final Long endTime = args.isNull(2)     ? null : args.getLong(2);
            final String type = args.isNull(3)      ? null : args.getString(3);
            final Long totalCount = args.isNull(4)  ? null : args.getLong(4);
            final Long totalBytes = args.isNull(5)  ? null : args.getLong(5);
            final String tag1 = args.isNull(6)      ? null : args.getString(6);
            final String tag2 = args.isNull(7)      ? null : args.getString(7);
            final String uriString = args.isNull(8) ? null : args.getString(8);
            final Boolean aborted = args.isNull(9)  ? null : args.getBoolean(9);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    NanovisorCordovaPlugin.this.reportEvent(startTime, t1Time, endTime, type, totalCount, totalBytes, tag1, tag2, uriString, aborted, callbackContext);
                }
            });
            return true;
        }
        return false;
    }

    private void startNanovisor(String license, Long dprSwitchArg, CallbackContext callbackContext) {
        if (license != null && license.length() > 0) {

            Nanovisor.setStatusListener(new NanovisorStatusListener() {
                @Override
                public void onAccelerationStateChanged(final boolean ready, final boolean accelerating, final int reasonCode, final String reasonMessage) {
                    Map<String, String> statusMap = new HashMap();
                    statusMap.put("ready", "" + ready);
                    statusMap.put("accelerating", "" + accelerating);
                    statusMap.put("reasonCode", "" + reasonCode);
                    statusMap.put("reasonMessage", "" + reasonMessage);
                    JSONObject status = new JSONObject(statusMap);

                    PluginResult result = new PluginResult(PluginResult.Status.OK, status);
                    result.setKeepCallback(true);
                    CallbackContext savedContext = NanovisorCordovaPlugin.this.statusListenerCallback;
                    if (savedContext != null) {
                        savedContext.sendPluginResult(result);
                    }
                }

                @Override
                public void onSessionIdChanged(final String newSessionID) {
                    PluginResult result = new PluginResult(PluginResult.Status.OK, newSessionID);
                    result.setKeepCallback(true);
                    CallbackContext savedContext = NanovisorCordovaPlugin.this.sessionIDListenerCallback;
                    if (savedContext != null) {
                        savedContext.sendPluginResult(result);
                    }
                }
            });

            Nanovisor.UseDPR dprSwitch = Nanovisor.UseDPR.DEFAULT;
            if (dprSwitchArg == 1) {
                dprSwitch = Nanovisor.UseDPR.ON;
            }
            else if (dprSwitchArg == 2) {
                dprSwitch = Nanovisor.UseDPR.OFF;
            }

            Application application = this.cordova.getActivity().getApplication();
            Nanovisor.configure(application, license).useDPR(dprSwitch).start();
            callbackContext.success();
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void getStatus(CallbackContext callbackContext) {
        NanovisorStatus status = Nanovisor.getStatus();
        Map<String, String> statusMap = new HashMap();
        statusMap.put("ready", "" + status.isReady());
        statusMap.put("accelerating", "" + status.isAccelerating());
        statusMap.put("reasonCode", "" + status.getReasonCode());
        statusMap.put("reasonMessage", "" + status.getReasonMessage());
        JSONObject json = new JSONObject(statusMap);
        callbackContext.success(json);
    }

    private void getSessionID(CallbackContext callbackContext) {
        String sessionID = Nanovisor.getSessionId();
        callbackContext.success(sessionID);
    }

    private void getNanovisorUrl(String urlString, CallbackContext callbackContext) {
        if (urlString != null && urlString.length() > 0) {
            try {
                URL url = new URL(urlString);
                callbackContext.success("" + Nanovisor.getNanovisorUrl(url));
            } catch (MalformedURLException ex) {
                callbackContext.error("URL is malformed.");
            }
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void reportEvent(Long startTime, Long t1Time, Long endTime, String type, Long totalCount, Long totalBytes, String tag1, String tag2, String uriString, Boolean aborted, CallbackContext callbackContext) {
        ObjectLoadEvent event = new ObjectLoadEvent();
        if (startTime != null) event.setStartTime(new Date(startTime));
        if (t1Time != null) event.setTime1(new Date(t1Time));
        if (endTime != null) event.setEndTime(new Date(endTime));
        if (type != null) event.setType(type.equals("o") ? ObjectLoadEvent.LoadType.OBJECT : ObjectLoadEvent.LoadType.PAGE);
        if (totalCount != null) event.setTotalCount(totalCount.intValue());
        if (totalBytes != null) event.setTotalBytes(totalBytes.intValue());
        if (tag1 != null) event.setTag(tag1);
        if (tag2 != null) event.setTag2(tag2);
        if (uriString != null) event.setUri(uriString);
        if (aborted != null) event.setAborted(aborted);
        
        try {
            Nanovisor.reportEvent(event);
            callbackContext.success("Report event succeeded.");
        } catch (EventException ex) {
            ex.printStackTrace();
            callbackContext.error("Report event failed. " + ex.getLocalizedMessage());
        }
    }

    private void setStatusChangeListener(CallbackContext callbackContext) {
        this.statusListenerCallback = callbackContext;
    }

    private void setSessionIDChangeListener(CallbackContext callbackContext) {
        this.sessionIDListenerCallback = callbackContext;
    }
}
