// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILReasonCodes.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 3/31/15.
//  Copyright (c) 2015 Kwicr. All rights reserved.
//

/*!
 *  @header ILReasonCodes
 *  @discussion Reason codes, that are returned to customer depending on acceleration state
 */

/*!
 *  @brief  Nanovisor is initializing
 */
extern NSInteger const ILReasonCodeInitializing;
/*!
 *  @brief  Nanovisor is accelerating
 */
extern NSInteger const ILReasonCodeAccelerationConnected;
/*!
 *  @brief  Connection with acceleration service failed
 */
extern NSInteger const ILReasonCodeAccelerationConnectFail;
/*!
 *  @brief  License is valid, but current session is unaccelerated
 */
extern NSInteger const ILReasonCodeAccelerationNotSelected;
/*!
 *  @brief  License is invalid
 */
extern NSInteger const ILReasonCodeServiceNotAuthorized;
/*!
 *  @brief  Connection with auth service failed
 */
extern NSInteger const ILReasonCodeServiceConnectFail;
/*!
 *  @brief  Connection is closing
 */
extern NSInteger const ILReasonCodeClosing;
/*!
 *  @brief  Nanovisor is stopped or never launched
 */
extern NSInteger const ILReasonCodeClosed;
