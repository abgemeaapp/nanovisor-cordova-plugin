// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILAnalytics.h
//  ILAnalytics
//
//  Created by Raajkumar Subramaniam on 5/5/14.
//  Copyright (c) 2014 QFactor Communications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILAnalyticsErrors.h"

/*!
 *  @header    ILAnalytics
 *  @brief     The public interface for reporting and setting up Nanovisor Analytics
 */


/*!
 *  Class for adding and removing monitors and reporting events
 *  to specified monitor with identifier
 */

NS_ASSUME_NONNULL_BEGIN;

@interface ILAnalytics : NSObject

/*!
 *  Method for reporting event without any metrics
 *
 *  @param type        type of event
 *  @param value       value of event
 *
 */
+ (void)reportEvent:(NSString*)type withValue:(id __nullable)value;

/*!
 *  Method for reporting event with metrics
 *
 *  @param type        type of event
 *  @param value       value of event
 *  @param metricsList dictionary of metrics
 *
 */
+ (void)reportEvent:(NSString*)type
          withValue:(id __nullable)value
         andMetrics:(NSDictionary* __nullable)metricsList;

/*!
 *  Method for reporting event with metrics and custom date
 *
 *  @param type        type of event
 *  @param value       value of event
 *  @param metricsList dictionary of metrics
 *  @param date        time when this event occured
 *
 */
+ (void)reportEvent:(NSString*)type
          withValue:(id __nullable)value
         andMetrics:(NSDictionary* __nullable)metricsList
            andDate:(NSDate*)date;

/*!
 *  SDK Version, example @"1.0"
 *
 *  @return build version for IL
 */
+ (NSString*)getSdkVersion;

/*!
 *  SDK Build number
 *
 *  @return build number for IL
 */
+ (NSNumber*)getSdkBuild;

/*!
 *  SDK name, example @"libNanovisorAnalytics"
 *
 *  @return name for IL
 */
+ (NSString*)getSdkName;

/*!
 *  @"release", "dev", "stag"
 *
 *  @return release level for IL
 */
+ (NSString*)getSdkReleaseLevel;

/*!
 *  SDK build date in format @"2014-07-20 21:30:00"
 *
 *  @return build date for IL
 */
+ (NSString*)getSdkBuildDate;

@end

NS_ASSUME_NONNULL_END;
