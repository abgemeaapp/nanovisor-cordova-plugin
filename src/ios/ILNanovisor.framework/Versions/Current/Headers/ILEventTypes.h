// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILEventTypes.h
//  ILAcceleration
//
//  Created by Susan Cudmore on 5/15/14.
//  Copyright (c) 2014 QFactor Communications. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @header    ILEventTypes.h
 *  @brief     Defines event types
 */

/*!
 *  @const IL_EVENTTYPE_SESSION_STARTED Event name for session start
 */
extern  NSString *const IL_EVENTTYPE_SESSION_STARTED;

/*!
 * @const IL_EVENTTYPE_SESSION_ENDED Event name for session end
 */
extern  NSString *const IL_EVENTTYPE_SESSION_ENDED;

/*!
 * @const IL_EVENTTYPE_SESSION_TIMER Event name for session timer
 */
extern  NSString *const IL_EVENTTYPE_SESSION_TIMER;

/*!
 * @const IL_EVENTTYPE_APPLICATION_FOREGROUNDED Event name to represent app moved to foreground
 */
extern  NSString *const IL_EVENTTYPE_APPLICATION_FOREGROUNDED;

/*!
 * @const IL_EVENTTYPE_APPLICATION_BACKGROUNDED Event name to represent app moved to background
 */
extern  NSString *const IL_EVENTTYPE_APPLICATION_BACKGROUNDED;

/*!
 * @const IL_EVENTTYPE_CONNECTION_STARTED Event name for connection start
 */
extern  NSString *const IL_EVENTTYPE_CONNECTION_STARTED;

/*!
 * @const IL_EVENTTYPE_CONNECTION_ENDED Event name for connection end
 */
extern  NSString *const IL_EVENTTYPE_CONNECTION_ENDED;

/*!
 * @const IL_EVENTTYPE_CONNECTION_ABORTED Event name connection abort
 */
extern  NSString *const IL_EVENTTYPE_CONNECTION_ABORTED;

/*!
 * @const IL_EVENTTYPE_CONNECTION_STAT Event name for connection stats
 */
extern  NSString *const IL_EVENTTYPE_CONNECTION_STAT;

/*!
 * @const IL_EVENTTYPE_HTTP_TRANSACTION Event name for http transaction
 */
extern  NSString *const IL_EVENTTYPE_HTTP_TRANSACTION;

/*!
 * @const IL_EVENTTYPE_VIDEO_REQUEST Event name for video request
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_REQUEST;

/*!
 * @const IL_EVENTTYPE_VIDEO_START Event name for video start
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_START;

/*!
 * @const IL_EVENTTYPE_VIDEO_END Event name for video end
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_END;

/*!
 * @const IL_EVENTTYPE_VIDEO_PAUSE Event name for video pause
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_PAUSE;

/*!
 * @const IL_EVENTTYPE_VIDEO_RESUME Event name for video resume
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_RESUME;

/*!
 * @const IL_EVENTTYPE_VIDEO_SEEK Event name for video seek
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_SEEK;

/*!
 * @const IL_EVENTTYPE_VIDEO_BANDWIDTH_CHANGED Event name for video bandwidth change
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_BANDWIDTH_CHANGED;

/*!
 * @const IL_EVENTTYPE_VIDEO_BANDWIDTH Event name for video bandwidth info
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_BANDWIDTH;

/*!
 * @const IL_EVENTTYPE_VIDEO_STALLED Event name for video stall
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_STALLED;

/*!
 * @const IL_EVENTTYPE_VIDEO_RECOVERED Event name for video recover
 */
extern  NSString *const IL_EVENTTYPE_VIDEO_RECOVERED;

/*!
 * @const IL_EVENTTYPE_LOCATION_CHANGED Event name for location change
 */
extern  NSString *const IL_EVENTTYPE_LOCATION_CHANGED;

/*!
 * @const IL_EVENTTYPE_MISC_CHANGE Event name for misc info change
 */
extern  NSString *const IL_EVENTTYPE_MISC_CHANGE;

/*!
 * @const IL_EVENTTYPE_CLICKED Event name for clicks
 */
extern  NSString *const IL_EVENTTYPE_CLICKED;

/*!
 * @const IL_EVENTTYPE_SHOWN Event name for app being shown
 */
extern  NSString *const IL_EVENTTYPE_SHOWN;

/*!
 * @const IL_EVENTTYPE_HIDDEN Event name for app being hidden
 */
extern  NSString *const IL_EVENTTYPE_HIDDEN;

/*!
 * @const IL_EVENTTYPE_DEBUG_ERROR Event name for any debug error info
 */
extern  NSString *const IL_EVENTTYPE_DEBUG_ERROR;

/*!
 * @const IL_EVENTTYPE_NETWORK_CHANGE Event name for network change event
 */
extern  NSString *const IL_EVENTTYPE_NETWORK_CHANGE;

/*!
 *  @brief  IL_EVENTTYPE_SERVICE_POLICY Event name for service policy change events
 */
extern NSString *const IL_EVENTTYPE_SERVICE_POLICY;
