//
//  ILAnalyticsErrors.h
//  ILAnalytics
//
//  Created by Dmitry Osipa on 11/10/15.
//  Copyright © 2015 Kwicr. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
 *  @brief  Analytics error domain
 */
extern NSString* const kILAnalyticsErrorDomain;
/*!
 *  @brief  Invalid event error code
 */
extern NSInteger const kILAnalyticsInvalidEventErrorCode;
/*!
 *  @brief  Analytics not initialized error code
 */
extern NSInteger const kILAnalyticsNotInitializedErrorCode;
