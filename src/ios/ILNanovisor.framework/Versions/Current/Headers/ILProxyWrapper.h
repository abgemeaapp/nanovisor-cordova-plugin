// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILProxyWrapper.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 8/26/14.
//  Copyright (c) 2014 QFactor Communications. All rights reserved.
//

#if defined __cplusplus
extern "C" {
#endif

/*!
 *  @header    ILProxyWrapper.h
 *  @brief     Functions for CFNetwork API.
 */

/*!
 *  @function ILRouteStreamForUrlWithNanovisor
 *  @brief Function for routing read stream via nanovisor.
 *  Checks if Nanovisor could route specified stream via kwikr.
 *  Then routes stream via kwikr on non-nanovisor proxy
 *
 *  @param stream read stream ref
 *  @param url    url ref
 */
void ILRouteStreamForUrlWithNanovisor(CFReadStreamRef stream, CFURLRef url);
/*!
 *  @function ILRouteStreamForRequestWithNanovisor
 *  @brief Function for routing read stream via nanovisor.
 *  Checks if Nanovisor could route specified stream via kwikr.
 *  Then routes stream via kwikr on non-nanovisor proxy.
 *
 *  @param stream read stream ref
 *  @param request     request message ref
 */
void ILRouteStreamForRequestWithNanovisor(CFReadStreamRef stream, CFHTTPMessageRef request);

#if defined __cplusplus
};
#endif
