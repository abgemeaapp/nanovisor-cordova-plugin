// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  NSURLRequest+NSURLRequest_Kwicr.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 1/6/15.
//  Copyright (c) 2015 Kwicr. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @header  NSURLRequest+NSURLRequest_Nanovisor.h
 *  @brief   Category for NSURLRequest that provides info on request's acceleration status
 */

@interface NSURLRequest (Nanovisor)

/*!
 *  @brief  Method for getting acceleration status of the request
 *
 *  @return YES if request is accelerated, NO otherwise
 */
- (BOOL)kwr_isAccelerated;

@end
