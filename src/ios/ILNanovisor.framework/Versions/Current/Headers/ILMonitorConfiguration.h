// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILMonitorConfiguration.h
//  ILAnalytics
//
//  Created by Alex Kudlay on 7/18/14.
//  Copyright (c) 2014 QFactor Communications. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @header    ILMonitorConfiguration.h
 *  @brief     Class to store and operate with monitoring configuration
 */

/*!
 *  Analytics configuration, typically recived from sin server. 
 *  Contains timers, level
 */

@interface ILMonitorConfiguration : NSObject <NSCoding>

/*!
 *  Indicates monitor should be enabled or disabled
 */
@property (nonatomic, strong) NSNumber* enabled;

/*!
 *  Unused
 */
@property (nonatomic, strong) NSNumber* level;

/*!
 *  Timer to resend failed batch
 */
@property (nonatomic, strong) NSNumber* timer_frequency;

/*!
 *  Timer to send all events in database
 */
@property (nonatomic, strong) NSNumber* flush_frequency;

/*!
 *  Analytics server address, in format "www.google.com"
 */
@property (nonatomic, strong) NSString* server;

/*!
 *  application session Id
 */
@property (nonatomic, strong) NSString* sessionId;

/*!
 *  The error description if any
 */
@property (nonatomic, strong) NSError* error;

/*!
 *  Initializer for configuration.
 *  Sets following default values
 *  enabled = @(NO);
 *  level = @(0);
 *  timer_frequency = @(0);
 *  flush_frequency = @(0);
 *  server = @"";
 *  sessionId = nil;
 *
 *  @return initialized configuration with default values
 */
- (instancetype)init NS_DESIGNATED_INITIALIZER;

/*!
 *  Initializer for configuration.
 *
 *  @param dict params dictionary from sin server
 *
 *  @return initialized configuration with sin params
 */
- (instancetype)initWithDict:(NSDictionary*)dict;

/*!
 *  Convinience method that returns server url
 *
 *  @return url constructed from server property
 */
@property (nonatomic, readonly, copy) NSURL *serverUrl;

/*!
 *  Convinience method to filter all urls
 *
 *  @return regexp string to filter all service urls
 */
@property (nonatomic, readonly, copy) NSString *serviceUrls;

@end

