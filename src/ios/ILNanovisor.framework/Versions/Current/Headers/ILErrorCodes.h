// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILErrorCodes.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 1/28/15.
//  Copyright (c) 2015 Nanovisor. All rights reserved.
//

/*!
 *  @header  ILErrorCodes
 *  @discussion Provides error codes for Nanovisor SDK
 */

/*!
 *  @brief  Domain for all Nanovisor SDK errors
 */
extern NSString* const ILErrorDomain;

/*!
 *  @brief Can't resolve server's hostname
 */
extern NSInteger const ILErrorCodeDNSError;

/*!
 *  @brief Nanovisor session tried to get established, but to response from server received
 */
extern NSInteger const ILErrorCodeConnectFailNoResponse;

/*!
 *  @brief Can't send requests in session.
 */
extern NSInteger const ILErrorCodeConnectFailCantSendRequest;

/*!
 *  @brief Nanovisor session was rejected by server
 */
extern NSInteger const ILErrorCodeRejected;

/*!
 *  @brief Nanovisor session was rejected by server due to high cpu or memory utilization
 */
extern NSInteger const ILErrorCodeRejectedNotEnoughtResource;

/*!
 *  @brief Nanovisor session was rejected by server because client and server versions did not match
 */
extern NSInteger const ILErrorCodeWrongVersion;

/*!
 *  @brief  Nanovisor session closed without notifying the server
 */
extern NSInteger const ILErrorCodeClosedWithError;

/*!
 *  @brief  Nanovisor failed to bind to a port
 */
extern NSInteger const ILErrorCodePortBindFailed;

/*!
 *  @brief  Nanovisor exited the accept loop
 */
extern NSInteger const ILErrorCodePortAcceptExited;

/*!
 *  @brief  Auth key is valid, but Acceleration for current session is disabled
 */
extern NSInteger const ILErrorCodeAccelerationNotSelected;
