// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILEvent.h
//  ILAnalytics
//
//  Created by Dmitry Osipa on 11/9/15.
//  Copyright © 2015 Kwicr. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @header    ILEvent.h
 *  @brief     Event is designed to provide a simple mechanism for tracking performance of a single page load over time. By comparing A/B cases with DPR and without DPR we can provide a measure of uplift during integration testing and also once live. By performing the same test often using a known set of pages we can provide SLA metrics using the same analytics.
 */

NS_ASSUME_NONNULL_BEGIN;

/*!
 Event type
 */
typedef enum {
    ILTypePage = 1,
    ILTypeObject
} ILType;

@interface ILEvent : NSObject

/*!
 *  @brief  1st timestamp in sequence, required
 */
@property (nonatomic, strong) NSDate* start_time;
/*!
 *  @brief  Generic support for intermediate "waypoints" in the page load timeline, optional
 */
@property (nonatomic, strong, nullable) NSDate* t1_time;
/*!
 *  @brief  Last timestamp in sequence, required
 */
@property (nonatomic, strong) NSDate* end_time;
/*!
 *  @brief  type of entity: object or page, required
 */
@property (nonatomic, assign) ILType type;
/*!
 *  @brief  Count of assets loaded by the page, e.g. js/css/img, etc., optional
 */
@property (nonatomic, strong, nullable) NSNumber* total_count;
/*!
 *  @brief  Total bytes for all assets loaded by the page, optional
 */
@property (nonatomic, strong, nullable) NSNumber* total_bytes;
/*!
 *  @brief  Original request url, optional
 */
@property (nonatomic, strong, nullable) NSString* uri;
/*!
 *  @brief  Short name for this page/object, required
 */
@property (nonatomic, strong) NSString* tag;
/*!
 *  @brief  Additional detail / info about the transaction, optional
 */
@property (nonatomic, strong, nullable) NSString* tag2;

/*!
 *  @brief  Is DPR on, required
 */
@property (nonatomic, strong, readonly) NSNumber* dpr;

/*!
 *  @brief  Yes if the load was aborted or has failed, optional
 */
@property (nonatomic, strong, nullable) NSNumber* aborted;

/*!
 *  @brief  Checks if event is valid
 *  @param  error error pointer
 *  @return YES, if event is valid
 */
- (BOOL)isValid:(NSError* __nullable __autoreleasing *)error;

@end

NS_ASSUME_NONNULL_END
