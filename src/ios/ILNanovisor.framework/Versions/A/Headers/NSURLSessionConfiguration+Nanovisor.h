// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  NSURLSessionConfiguration+Kwicr.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 7/24/14.
//  Copyright (c) 2014 QFactor Communications. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @header    NSURLSessionConfiguration+Nanovisor
 *  @brief     Extension for NSURLSessionConfiguration to support Nanovisor.
 * Since NSURLSessionConfiguration containts it's own mechanism
 * for registered NSURLProtocol subclasses. So ILUrlProtocol should
 * be registered in NSURLSession configuration. Which is basically
 * done in this category.
 * -nanovisorConfiguration should be used in applications with NSURLSession
 * instead of -defaultSessionConfiguration method.
 */


@interface NSURLSessionConfiguration (Nanovisor)

/*!
 *  Initializer for Nanovisor enabled session configuration
 *
 *  @return Nanovisor enabled session configuration
 */
+ (NSURLSessionConfiguration*)nanovisorConfiguration;

@end
