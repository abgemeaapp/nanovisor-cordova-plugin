//
//  ILAnalytics+ILEventSupport.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 12/3/15.
//  Copyright © 2015 Kwicr. All rights reserved.
//

//#import <ILNanovisor.h>
#import "ILAnalytics.h"
#import "ILEvent.h"


NS_ASSUME_NONNULL_BEGIN

/*!
 *  @header    ILAnalytics (ILEventSupport)
 *  @brief     Extension to support ILEvent object
 */

@interface ILAnalytics (ILEventSupport)

/*!
 *  @brief  Custom analytics API
 *
 *  @param event event object
 *  @param error error on reporting event
 *
 *  @return YES, if event successfully reported
 */
+ (BOOL)reportEvent:(ILEvent*)event error:(NSError* __nullable __autoreleasing *)error;

@end

NS_ASSUME_NONNULL_END
