// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  ILNanovisor.h
//  ILAcceleration
//
//  Created by Raajkumar Subramaniam on 11/26/13.
//  Copyright (c) 2013 QFactor. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @header    ILNanovisor.h
 *  @brief     Acceleration Service class should be used to start and stop Nanovisor
 *             acceleration service
 */

/*!
 *  @brief This notification will be invoked when the session ID changes.
 */
extern NSString* ILNanovisorSessionIDChangedNotification;

/*!
 *  @brief New sessionID key in userInfo dictionary of ILNanovisorSessionIDChangedNotification notification
 */
extern NSString* ILNanovisorSessionIDKey;

/*!
 *  @brief This notification will be invoked when the acceleration state has changed.
 *  userInfo dictionary will contain new acceleration state from ILAccelerationState enum.
 */
extern NSString* ILNanovisorStateChangedNotification;

/*!
 *  @brief Key for userInfo dictionary with SDK state. Contains NSNumber with bool value. YES if SDK is ready to use, NO otherwise
 */
extern NSString* ILNanovisorStateKey;

/*!
 *  @brief Key for userInfo dictionary with new acceleration state.  Contains NSNumber with bool value. YES if app traffic is accelerated, NO otherwise
 */
extern NSString* ILAccelerationStateKey;

/*!
 *  @brief  Key for userInfo dictionary with reason code. May be not present.
 */
extern NSString* ILAccelerationStateReasonKey;

/*!
 *  @brief  Key for userInfo dictionary with error. May be not present.
 */
extern NSString* ILAccelerationStateErrorKey;

/*!
 *  @brief  Key for userInfo dictionary with reason message. May be not present.
 */
extern NSString* ILAccelerationStateReasonMessageKey;

typedef enum : NSUInteger {
    ILAccelerationSwitchDefault,
    ILAccelerationSwitchOn,
    ILAccelerationSwitchOff
} ILAccelerationSwitch;

typedef void (^ILNanovisorChallengeHandlerBlock)(NSURLAuthenticationChallenge*, void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential));

@interface ILNanovisor: NSObject

/*!
 *  @brief  indicates SDK state
 */
@property (nonatomic, assign, readonly, getter=isNanovisorReady) BOOL nanovisorReady;

/*!
 *  @brief  indicates Acceleration state for session
 */
@property (nonatomic, assign, readonly, getter=isNanovisorAccelerating) BOOL nanovisorAccelerating;

/*!
 *  @brief indicates current status of Nanovisor
 */
@property (nonatomic, assign, readonly) NSInteger reasonCode;

/*!
 *  @brief indicates current status message of Nanovisor
 */
@property (nonatomic, strong, readonly) NSString* reasonMessage;

/*!
 *  @brief returns current session ID
 */
- (NSString*)sessionID;

/*! Configures the calling application so that all network operations originating from the application are Nanovisor enabled.
 @param developerKey unique identifier assigned to this developer account at the time of registration.
 */
- (void)startNanovisor: (NSString *)developerKey;

/*! Configures the calling application so that all network operations originating from the application are Nanovisor enabled.
 @param developerKey unique identifier assigned to this developer account at the time of registration.
 @param accelerationSwitch switch to enable/disable the acceleration. Traffic monitoring state is not affected
*/
- (void)startNanovisor: (NSString *)developerKey withAccelerationSwitch:(ILAccelerationSwitch)accelerationSwitch;

/*! Configures the calling application so that the network operations are no longer enhanced using Nanovisor techonology. */
- (void)stopNanovisor;

/*! Sets NSURLCredential to be used in didReceiveChallenge when communicating to specified domain name or subdomains
 */
- (void)setCredential:(NSURLCredential*)credential forDomain:(NSString*)domain;

/*! Disables certificate validation for domain.
 */
- (void)trustDomain:(NSString*)domain;

/*! Sets the block that will be invoked from didReceiveChallenge of NSURLSession when resolving
 *  challenges for specified domain
 */
- (void)setCustomChallengeHandlerBlock:(ILNanovisorChallengeHandlerBlock)customChallengeHandlerBlock forDomain:(NSString*)domain;

/*!
 * Shared reference to Nanovisor acceleration API
 */
+ (ILNanovisor*)sharedInstance;

@end
