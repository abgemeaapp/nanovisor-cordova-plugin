// COPYRIGHT BEGIN
// *************************************************************************
// Q Factor Communications Corp and KWICR, proprietary and confidential
//
// Copyright(C) Q Factor Communications Corp. 2015
// All Rights Reserved.
//
// NOTICE: All information contained herein is, and remains
// the property of Q Factor Communications Corp and its suppliers,
// if any. The intellectual and technical concepts contained
// herein are proprietary to Q Factor Communications Corp and its
// suppliers and may be covered by U.S. and Foreign Patents, patents
// in process, and are protected by trade secret or copyright law.
// Dissemination of this information or reproduction of this material
// is strictly forbidden unless prior written permission is obtained
// from Q Factor Communications Corp.
// *************************************************************************
// COPYRIGHT END
//
//  NSURL+Nanovisor.h
//  ILAcceleration
//
//  Created by Dmitry Osipa on 3/5/15.
//  Copyright (c) 2015 Kwicr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Nanovisor)

- (NSURL*)nanovisorUrl;

@end
