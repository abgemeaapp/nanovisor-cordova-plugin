/********* NanovisorCordovaPlugin.m Cordova Plugin Implementation *******/

#import "NanovisorCordovaPlugin.h"

#import <Cordova/CDV.h>

#import <ILNanovisor/ILNanovisor.h>
#import <ILNanovisor/NSURL+Nanovisor.h>

@interface NanovisorCordovaPlugin ()
    
@property (nonatomic, strong) NSString* statusChangeCallbackId;
@property (nonatomic, strong) NSString* sessionIDChangeCallbackId;
    
@end

@implementation NanovisorCordovaPlugin
    
- (void)startNanovisor:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* license = [command.arguments objectAtIndex:0];
        NSNumber* dprSwitchArg = [command.arguments objectAtIndex:1];
        
        if (license != nil && [license length] > 0) {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(nanovisorStateChangedNotification:)
                                                         name:ILNanovisorStateChangedNotification
                                                       object:nil];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(sessionIdChangedNotification:)
                                                         name:ILNanovisorSessionIDChangedNotification
                                                       object:nil];
            
            ILAccelerationSwitch dprSwitch = ILAccelerationSwitchDefault;
            if (dprSwitchArg.intValue == 1) {
                dprSwitch = ILAccelerationSwitchOn;
            }
            else if (dprSwitchArg.intValue == 2) {
                dprSwitch = ILAccelerationSwitchOff;
            }

            [[ILNanovisor sharedInstance] startNanovisor:license withAccelerationSwitch:dprSwitch];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getStatus:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        NSDictionary* dictionary = @{ @"ready" : [ILNanovisor sharedInstance].isNanovisorReady ? @1:@0,
                                      @"accelerating" : [ILNanovisor sharedInstance].isNanovisorAccelerating ? @1:@0,
                                      @"reasonCode" : @([ILNanovisor sharedInstance].reasonCode),
                                      @"reasonMessage" : [ILNanovisor sharedInstance].reasonMessage,
                                      };

        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dictionary];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}
    
- (void)getSessionID:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        NSString* sessionID = [[ILNanovisor sharedInstance] sessionID];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:sessionID];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getNanovisorUrl:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* urlString = [command.arguments objectAtIndex:0];
        
        if (urlString != nil && [urlString length] > 0) {
            NSURL* url = [NSURL URLWithString:urlString];
            NSURL* nanovisorUrl = [url nanovisorUrl];
            NSString* nanovisorUrlString = [nanovisorUrl absoluteString];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:nanovisorUrlString];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}
    
- (void)setStatusChangeListener:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        self.statusChangeCallbackId = command.callbackId;
    }];
}

- (void)setSessionIDChangeListener:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        self.sessionIDChangeCallbackId = command.callbackId;
    }];
}

- (BOOL)isNull:(id)obj
{
    if (obj == nil || obj == [NSNull null]) {
        return true;
    }
    return false;
}

- (void)reportEvent:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        ILEvent* event = [ILEvent new];
        
        id obj = [command.arguments objectAtIndex:0];
        if (![self isNull:obj]) {
            double startTimeMillis = [obj doubleValue];
            NSDate* startDate = [NSDate dateWithTimeIntervalSince1970:startTimeMillis/1000];
            event.start_time = startDate;
        }
        
        obj = [command.arguments objectAtIndex:1];
        if (![self isNull:obj]) {
            double t1TimeMillis = [obj doubleValue];
            NSDate* t1Date = [NSDate dateWithTimeIntervalSince1970:t1TimeMillis/1000];
            event.t1_time = t1Date;
        }
        
        obj = [command.arguments objectAtIndex:2];
        if (![self isNull:obj]) {
            double endTimeMillis = [[command.arguments objectAtIndex:2] doubleValue];
            NSDate* endDate = [NSDate dateWithTimeIntervalSince1970:endTimeMillis/1000];
            event.end_time = endDate;
        }
        
        obj = [command.arguments objectAtIndex:3];
        if (![self isNull:obj]) {
            NSString* typeString = [command.arguments objectAtIndex:3];
            ILType type = [typeString isEqualToString:@"o"] ? ILTypeObject : ILTypePage;
            event.type = type;
        }
        
        obj = [command.arguments objectAtIndex:4];
        if (![self isNull:obj]) {
            event.total_count = obj;
        }
        
        obj = [command.arguments objectAtIndex:5];
        if (![self isNull:obj]) {
            event.total_bytes = obj;
        }
        
        obj = [command.arguments objectAtIndex:6];
        if (![self isNull:obj]) {
            event.tag = obj;
        }
        
        obj = [command.arguments objectAtIndex:7];
        if (![self isNull:obj]) {
            event.tag2 = obj;
        }
        
        obj = [command.arguments objectAtIndex:8];
        if (![self isNull:obj]) {
            event.uri = obj;
        }
        
        obj = [command.arguments objectAtIndex:9];
        if (![self isNull:obj]) {
            event.aborted = obj;
        }
        
        NSError* error = nil;
        [ILAnalytics reportEvent:event error:&error];
        
        CDVPluginResult* pluginResult = nil;
        if (error == nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        }
        else {
            NSString* message = [NSString stringWithFormat:@"Failed to report event. %@", error.localizedDescription];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}
    
- (void)sendStatusUpdateToJS:(NSDictionary*)status
{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:status];
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.statusChangeCallbackId];
}

- (void)sendSessionIDUpdateToJS:(NSString*)sessionID
{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:sessionID];
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.sessionIDChangeCallbackId];
}
    
- (void)nanovisorStateChangedNotification:(NSNotification*)notification {
    BOOL sdkReady    = [notification.userInfo[ILNanovisorStateKey] boolValue];
    BOOL accelerating = [notification.userInfo[ILAccelerationStateKey] boolValue];
    NSUInteger reasonCode = [notification.userInfo[ILAccelerationStateReasonKey] integerValue];
    NSString* reasonMessage = notification.userInfo[ILAccelerationStateReasonMessageKey];
    
    NSDictionary* status = @{@"ready":@(sdkReady),
                             @"accelerating":@(accelerating),
                             @"reasonCode":@(reasonCode),
                             @"reasonMessage":reasonMessage
                             };
    
    [self sendStatusUpdateToJS:status];
}
    
- (void)sessionIdChangedNotification:(NSNotification*)notification {
    NSString* sessionID = notification.userInfo[ILNanovisorSessionIDKey];
    [self sendSessionIDUpdateToJS:sessionID];
}

@end

