/********* NanovisorCordovaPlugin.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface NanovisorCordovaPlugin : CDVPlugin {
  // Member variables go here.
}

@property (nonatomic, readonly) NSString* statusChangeCallbackId;

- (void)startNanovisor:(CDVInvokedUrlCommand*)command;
- (void)getStatus:(CDVInvokedUrlCommand*)command;
- (void)getSessionID:(CDVInvokedUrlCommand*)command;
- (void)getNanovisorUrl:(CDVInvokedUrlCommand*)command;
- (void)setStatusChangeListener:(CDVInvokedUrlCommand*)command;
- (void)setSessionIDChangeListener:(CDVInvokedUrlCommand*)command;
- (void)reportEvent:(CDVInvokedUrlCommand*)command;

@end
