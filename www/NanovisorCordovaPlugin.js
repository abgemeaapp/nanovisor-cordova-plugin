var exec = require('cordova/exec');

var PLUGIN_NAME = 'NanovisorCordovaPlugin';

var NanovisorCordovaPlugin = {
  startNanovisor: function(arg0, success) {
    exec(success, null, PLUGIN_NAME, 'startNanovisor', [arg0, 0]);
  },
  startNanovisorWithAccelerationOn: function(arg0, success) {
    exec(success, null, PLUGIN_NAME, 'startNanovisor', [arg0, 1]);
  },
  startNanovisorWithAccelerationOff: function(arg0, success) {
    exec(success, null, PLUGIN_NAME, 'startNanovisor', [arg0, 2]);
  },
  getStatus: function(success) {
    exec(success, null, PLUGIN_NAME, 'getStatus', []);
  },
  getSessionID: function(success) {
    exec(success, null, PLUGIN_NAME, 'getSessionID', []);
  },
  getNanovisorUrl: function(arg0, success) {
    exec(success, null, PLUGIN_NAME, 'getNanovisorUrl', [arg0]);
  },
  setStatusChangeListener: function(success) {
    exec(success, null, PLUGIN_NAME, 'setStatusChangeListener', []);
  },
  setSessionIDChangeListener: function(success) {
    exec(success, null, PLUGIN_NAME, 'setSessionIDChangeListener', []);
  },
  reportEvent: function(startTime, t1Time, endTime, type, totalCount, totalBytes, tag1, tag2, uri, aborted, success, failure) {
    exec(success, failure, PLUGIN_NAME, 'reportEvent', [startTime, t1Time, endTime, type, totalCount, totalBytes, tag1, tag2, uri, aborted]);
  },
  reportEvent: function(event, success, failure) {
    exec(success, failure, PLUGIN_NAME, 'reportEvent', [event.startTime, event.t1Time, event.endTime, event.type, event.totalCount, event.totalBytes, event.tag1, event.tag2, event.uri, event.aborted]);
  },

  NanovisorEvent: function(startTime, endTime, type, tag1) {
    if (!(this instanceof NanovisorCordovaPlugin.NanovisorEvent)) { 
      return new NanovisorCordovaPlugin.NanovisorEventt(startTime, endTime, type, tag1);
    }
    this.startTime = startTime;
    this.endTime = endTime;
    this.type = type;
    this.tag1 = tag1;
    return this;
  }
};

module.exports = NanovisorCordovaPlugin;
